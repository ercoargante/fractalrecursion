/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractraltree;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
 
public class FractalTree extends JFrame {
        
    public FractalTree() {
        super("Fractal Tree");
        init();
    }
    
    private void init() {
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
 
    private void drawTree(Graphics g, int currentX, int currentY, double angle, int depth) {
        if (depth == 0) return;
        double length = depth * 7.0;
        int newX = currentX + (int) (Math.cos(Math.toRadians(angle)) * length);
        int newY = currentY + (int) (Math.sin(Math.toRadians(angle)) * length);
        g.drawLine(currentX, currentY, newX, newY);
        drawTree(g, newX, newY, angle - 20, depth - 1);
        drawTree(g, newX, newY, angle + 20, depth - 1);
    }
     
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        drawTree(g, 400, 500, -90, 9);
    }
 
    public static void main(String[] args) {
        new FractalTree().setVisible(true);
    }
}